## Installation 

```bash
composer require liukai/grid-imagebox

php artisan vendor:publish --tag=laravel-admin-grid-viewer
```

## Configurations

Open `config/admin.php`, add configurations that belong to this extension at `extensions` section.
```php

    'extensions' => [

        'grid-imagebox' => [
        
            // Set to `false` if you want to disable this extension
            'enable' => true,
        ]
    ]

```

## Usage

Use it in the grid:
```php
// simple lightbox
$grid->picture()->imagebox();


//zoom effect
$grid->picture()->imagebox(['zooming' => true]);

//width & height properties
$grid->picture()->imagebox(['width' => 50, 'height' => 50]);

//img class properties
$grid->picture()->imagebox(['class' => 'rounded']);
```

License
------------
Licensed under [The MIT License (MIT)](LICENSE).


<?php

namespace Encore\Grid\ImageBox;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Displayers\AbstractDisplayer;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class ImageBoxDisplayer extends AbstractDisplayer
{
    protected function script()
    {
        return <<<SCRIPT

    const img = $('tbody');
    const viewer = new Viewer(img[0],{
        zIndex:99999999,
        title:false
    });

SCRIPT;
    }
    public function display(array $options = [])
    {
        if (empty($this->value)) {
            return '';
        }
        if ($this->value instanceof Arrayable) {
            $this->value = $this->value->toArray();
        }

        $server = Arr::get($options, 'server');
        $width = Arr::get($options, 'width', 200);
        $height = Arr::get($options, 'height', 200);
        $class = Arr::get($options, 'class', 'thumbnail');
        $class = collect((array)$class)->map(function ($item) {
            return 'img-'. $item;
        })->implode(' ');

        Admin::script($this->script());

        return collect((array)$this->value)->filter()->map(function ($path) use ($server, $width, $height, $class) {
            if (url()->isValidUrl($path) || strpos($path, 'data:image') === 0) {
                $src = $path;
            } elseif ($server) {
                $src = rtrim($server, '/') . '/' . ltrim($path, '/');
            } else {
                $src = Storage::disk(config('admin.upload.disk'))->url($path);
            }

            return <<<HTML
    <img data-original='$src' src='$src' style='max-width:{$width}px;max-height:{$height}px' class='grid-popup-link img {$class}' />
HTML;
        })->implode('&nbsp;');
    }
}

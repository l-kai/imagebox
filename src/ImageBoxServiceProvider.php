<?php

namespace Encore\Grid\ImageBox;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Column;
use Illuminate\Support\ServiceProvider;

class ImageBoxServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(ImageBox $extension)
    {
        if (! ImageBox::boot()) {
            return ;
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/laravel-admin-ext/grid-viewer')],
                'laravel-admin-grid-viewer'
            );
        }

        Admin::booting(function () {

            Admin::css('vendor/laravel-admin-ext/grid-viewer/viewer.css');
            Admin::js('vendor/laravel-admin-ext/grid-viewer/viewer.js');

            Column::extend('imagebox', ImageBoxDisplayer::class);
        });
    }
}

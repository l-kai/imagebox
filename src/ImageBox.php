<?php

namespace Encore\Grid\ImageBox;

use Encore\Admin\Extension;

class ImageBox extends Extension
{
    public $name = 'grid-imagebox';

    public $assets = __DIR__.'/../resources/assets';
}
